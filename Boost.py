#! /usr/bin/env python
import os
import enchant
from openpyxl import Workbook
from openpyxl.reader.excel import load_workbook
from openpyxl.cell import Cell
from openpyxl.styles import Alignment, Font

#Bird, Steven, Edward Loper and Ewan Klein (2009), Natural Language Processing with Python.  O'Reilly Media Inc.
import nltk
from nltk.stem import WordNetLemmatizer


class Boost:
    def __init__(self, question):

        #Load TrainingSet.xlsx
        wb    = load_workbook('TrainingSet.xlsx')
        ws    = wb['history']
        total = wb['totals']['A2'].value + 1

        # Lists of tuples for each category.
        # One tuple contains an individual words and the frequency with which it occurs in that category.
        self.cat1 = []
        self.cat2 = []
        self.cat3 = []
        self.cat4 = []
        
        # 1 - Defect (usability)
        # 2 - Defect (visualization)
        # 3 - Documentation
        # 4 - Feature Requests
        # 5 - Other

        # Takes questions from TestSet.xlsx and puts them in the Training Set.
        # "train" is a list of tuples containing questions and the categories to which they belong.
        self.train = []
        for i in range(2, total+1):
            val = 'A' + str(i)
            key = 'B' + str(i)
            self.train.append((ws[val].value, ws[key].value))
        
        # Train using the training set
        for i in range(len(self.train)):
            self.addToCategory(self.train[i][0], self.train[i][1])
        
        
        # Get a list of fitness scores for each category
        fit = self.rule(question)
        self.c = 0
        print fit
        
        # Find max fitness score across all categories, add question into appropriate category
        # If multiple fitness scores are equal, then the question will be placed into category 5 for other
        if fit[0] == fit[1] == fit[2] == fit[3]:
            print 'Category 5'
            self.c
        else:
            num = 0
            maxNum = max(fit)
            for j in range(len(fit)):
                if fit[j] == maxNum:
                    num = j+1
            print 'Category', num
            self.c = num
        


    # Converts the sring of the question into a list of keywords that are contained in the question.
    # Parameters: the question (as a string)
    # Return: list of keywords in the question
    def convertString(self, question):
        str1 = nltk.word_tokenize(question)
        list1 = nltk.pos_tag(str1)
        list2 = []
        d = enchant.Dict('en_US')
        w = WordNetLemmatizer()
        for i in range(len(list1)):
            if (list1[i][1] == 'JJ' \
                or list1[i][1] == 'JJR' \
                or list1[i][1] == 'JJS' \
                or list1[i][1] == 'NN' \
                or list1[i][1] == 'NNS' \
                or list1[i][1] == 'NNP' \
                or list1[i][1] == 'NNPS' \
                or list1[i][1] == 'VB' \
                or list1[i][1] == 'VBD' \
                or list1[i][1] == 'VBG' \
                or list1[i][1] == 'VBN' \
                or list1[i][1] == 'VBP' \
                or list1[i][1] == 'VBZ') \
                and d.check(list1[i][0]):
                list2.append(w.lemmatize(list1[i][0].lower()))
        return list2


    # Trains the specified category with the question
    # Parameters: The question and the category it belongs in
    def addToCategory(self, question, category):
        x = self.convertString(question)

        if category == 1:
            if len(self.cat1) == 0:
                for i in range(len(x)):
                    self.cat1.append((x[i], 1))
            for i in range(len(x)):
                isFin = True
                for j in range(len(self.cat1)):
                    if x[i] == self.cat1[j][0]:
                        self.cat1[j] = (self.cat1[j][0], self.cat1[j][1] + 1)
                        isFin = False
                if isFin:
                        self.cat1.append((x[i], 1))

        elif category == 2:
            if len(self.cat2) == 0:
                for i in range(len(x)):
                    self.cat2.append((x[i], 1))
            for i in range(len(x)):
                isFin = True
                for j in range(len(self.cat2)):
                    if x[i] == self.cat2[j][0]:
                        self.cat2[j] = (self.cat2[j][0], self.cat2[j][1] + 1)
                        isFin = False
                if isFin:
                    self.cat2.append((x[i], 1))

        elif category == 3:
            if len(self.cat3) == 0:
                for i in range(len(x)):
                    self.cat3.append((x[i], 1))
            for i in range(len(x)):
                isFin = True
                for j in range(len(self.cat3)):
                    if x[i] == self.cat3[j][0]:
                        self.cat3[j] = (self.cat3[j][0], self.cat3[j][1] + 1)
                        isFin = False
                if isFin:
                    self.cat3.append((x[i], 1))
        elif category == 4:
            if len(self.cat4) == 0:
                for i in range(len(x)):
                    self.cat4.append((x[i], 1))
            for i in range(len(x)):
                isFin = True
                for j in range(len(self.cat4)):
                    if x[i] == self.cat4[j][0]:
                        self.cat4[j] = (self.cat4[j][0], self.cat4[j][1] + 1)
                        isFin = False
                if isFin:
                    self.cat4.append((x[i], 1))

    
    # Returns the number corresponding with the correct category (returns 1,2,3 for categories 1,2,3)
    def getCat(self):
        return self.c
    
    # Returns a combined list of all of the words from each category
    def getAllCat(self):
        x = self.cat1
        for i in range(len(self.cat2)):
            isFin = True
            for j in range(len(x)):
                if x[j][0] == self.cat2[i][0]:
                    x[j] = (x[j][0], x[j][1] + 1)
                    isFin = False
            if isFin:
                    x.append(self.cat1[i])
        for i in range(len(self.cat3)):
            isFin = True
            for j in range(len(x)):
                if x[j][0] == self.cat3[i][0]:
                    x[j] = (x[j][0], x[j][1] + 1)
                    isFin = False
            if isFin:
                    x.append(self.cat3[i])
        for i in range(len(self.cat4)):
            isFin = True
            for j in range(len(x)):
                if x[j][0] == self.cat4[i][0]:
                    x[j] = (x[j][0], x[j][1] + 1)
                    isFin = False
            if isFin:
                    x.append(self.cat4[i])
        for i in range(len(x)):
            x[i] = (x[i][0].encode('ascii', 'ignore'), x[i][1])
        return x


    # If rule.txt is not created, create it.  Then, test each rule against the question to generate a fitness score for each category
    # Parameters: the question (as a string)
    # Returns a list of all of the fitness scores for each category
    def rule(self, question):
        x = self.getAllCat()
        fCat1 = 0
        fCat2 = 0
        fCat3 = 0
        fCat4 = 0
        if not (os.path.exists('rule.txt')):
            self.create()
        
        rulesFile = open('rule.txt', 'r+')
        rules = rulesFile.readlines()
        ruleList = []
        q = self.convertString(question)
        for i in range(len(rules)):
            token = nltk.word_tokenize(rules[i])
            print 'Load Rule: ', token
            ruleList.append(token)
        
        for i in range(len(ruleList)):
            percent = float(ruleList[i][3])
            if int(ruleList[i][0]) == 2:
                percent *= -1
            fit = self.fitness(q, ruleList[i][1], percent)
            category = int(ruleList[i][2])
            if category == 1:
                fCat1 += fit
            elif category == 2:
                fCat2 += fit
            elif category == 3:
                fCat3 += fit
            else:
                fCat4 += fit
        return [fCat1, fCat2, fCat3, fCat4]

    # Creates a rule.txt document and generates rules which it writes to rule.txt
    def create(self):
        x = self.getAllCat()
        doc = open('rule.txt', 'w')
        counter = len(x) * 4
        for i in range(len(x)):
            for j in range(4):
                word = x[i][0]
                category = j+1
                percent = self.test(word, category)
                rule = ''
                if percent >= 0:
                    rule += '1 ' + word + ' ' + str(category) + ' ' + str(abs(percent)) + '\n'
                    print '1 ' + word + ' ' + str(category) + ' ' + str(abs(percent))
                else:
                    rule += '2 ' + word + ' ' + str(category) + ' ' + str(abs(percent)) + '\n'
                    print '2 ' + word + ' ' + str(category) + ' ' + str(abs(percent))
                print counter
                counter -= 1
                doc.write(rule)
        doc.close()

    # Tests the accuracy of a specifc word in the category
    # Parameters: the word and the category to test in
    # Return a float percent
    def test(self, word, category):
        correct = 0
        total = 0
        for i in range(len(self.train)):
            x = self.convertString(self.train[i][0])
            for j in range(len(x)):
                if x[j] == word:
                    total += 1
                    if self.train[i][1] == category:
                        correct += 1
        
        percent1 = float(correct)/float(total)
        percent = 0
        if percent1 > .25:
            percent = (percent1 - .25) * 1.333333333
        else:
            percent = (1 - (4 * percent1)) * -1
        return round(percent, 2)
    
    # Finds the fitness of a specific rule on a question
    # Parameters: The question and information from a specific rule (word, category, and percent accuracy)
    # Returns fitness of the rule
    def fitness(self, question, word, percent):
        fitness = 0.0
        for i in range(len(question)):
            if question[i] == word:
                if percent < 0:
                    fitness += percent * 0.333333333
                else:
                    fitness += percent
        
        return fitness